package account

import input

default allow = false

token = {"payload": payload} { io.jwt.decode(input.token, [_, payload, _]) }

valid_request_v1 {
    input.apiVersion == "v1"
    # input.issuer = "daf"    
}

#Employees
allow  {
    valid_request_v1
    token.payload.usertype == "employee"
    allowed_roles = ["banker","admin"]
    token.payload.roles[_] == allowed_roles[_]
    # TODO How should this be split up if bankers and admins should have different permissions? different allow blocks?    
    valid_actions := ["Account.List", "Account.Block"]
    input.action.actionId == valid_actions[_]
}

#Customers
allow  {
    valid_request_v1
    token.payload.usertype = "customer"
    permissions[_].userid == token.payload.user
    # TODO check the actual permissions
}

                                                 

# The set of IDs for all permitted resources
resources[r] {
    
    ## from bulk loaded model
    # r := resources.resources[_].id  

    ## From API model
    r := permissions[_].resources[_].id
}

## Expecting fetch endpoint to give this structure
# "apiVersion": "v1",                                                          
# "kind":"UserPermissions",                                                    
# "domain":"accounts",                                                         
# "userid":"jane",                                                             
# "resources":[                                                                
#     { "kind":"Account", "id":"jane-acct1", "actions":["Account.Read"] },     
#     { "kind":"Account", "id":"jane-acct2", "actions":["Account.Read"] },     
#     { "kind":"Account", "id":"jane-acct3", "actions":["Account.Read"] }      
# ],                                                                           
# "actions":["Account.List"]  
permissions[p] {
    p := fetch_permissions(token.payload.user)
    p.apiVersion == "v1"
    p.kind == "UserPermissions"
}


fetch_permissions(user) = r {
    url := sprintf("http://localhost:8888/api/v1/pip/accounts/user/%s/permissions.json", [user])
    trace(url)
    # url := "https://jsonblob.com/api/79ad1089-5368-11e9-a4ea-4bc62f672941"

    http_response := http.send(
        {   "method": "get",
            "url":url
        }
    )
    trace(sprintf("RESPONSE %v", [http_response]))
    r := http_response.body
}

## Example code, can be removed
# fetch_acls(_) = acls {
#     # pretend this is the http.send call. http.send is not allowed inside the playground.
# 	output := {"body": {"acls": ["a", "b", "c"]}}
#     acls := {x | x := output.body.acls[_]}
# }

# test {
# 	fetch_acls("a") 
# }
