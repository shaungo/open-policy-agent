As a reminder, there are three Levels of PDP Access Control:
 Level 1: Authentication Access - Allow all actions to every signed in user and no actions to an anonymous user.
 Level 2: Basic Authorization - Check which resources and verbs the currently logged in user should have access to
 Level 3: Advanced Authorization - Fine grained control through XACML



XACML decision POST is to .../pdp endpoint 
it passes a structure of Category & List of Attributes

* access-subject 
** subject-id = joe
** role = Manager
* resource
** resource-id = MissionMangementApp
** sub-resource-id = Team
* action
** action-id = manage
* environment



XACML returns <Result><Decision>Permit</Decision></Result>




Attribute providers: customize the way attribute value are retrieved outside the PEP’s Request
The interface for AttributeProvider is
- get(AttributeFQN, DataType, EvaluationContext )

Authzforce has:
- AttributeProviders
- PolicyProviders


https://github.com/FIWARE/tutorials.XACML-Access-Rules

Any PEP consists of two parts 
1. OAuth request to IDP /user endpoint to retrieve roles etc
2. PDP call with user info + call context


FIWARE/Authzforce uses an bearer reference token, when getting user info for policies they do
GET http://localhost:3005/user?access_token={{access_token}}&app_id={{app-id}}   => 
Gives:
{
    "organizations": [],
    "displayName": "",
    "roles": [
        {
            "id": "managers-role-0000-0000-000000000000",
            "name": "Management"
        }
    ],
    "app_id": "tutorial-dckr-site-0000-xpresswebapp",
    "trusted_apps": [],
    "isGravatarEnabled": false,
    "email": "bob-the-manager@test.com",
    "id": "bbbbbbbb-good-0000-0000-000000000000",
    "authorization_decision": "",
    "app_azf_domain": "gQqnLOnIEeiBFQJCrBIBDA",
    "eidas_profile": {},
    "username": "bob"
}