


Need to policy types for:
* Resource - Which USERs can do what ACTIONs on a RESOURCE
    RESOURCE => USER[ ACTION[] ]
* User - Which RESOURCEs can USER to what ACTIONs on 
    USER => RESOURCE[ ACTION[] ]


# Goal
I want to be able to have the PDP (OPA policy rego) respond to the section of the PIP model that is missing
This would be returned and resolved by the outer service, then provided into OPA for another invocation.
This is sort of like an attribute provider in XACML
The primary goal is for the policy to inform what additional information is needed, not have an additional component know.
In this way, the information is only retrieve if it is needed, and also encapsulated in the policy document
Lastly, the policy document doesn't contain where the data is, this could be resolved differently depending on the context (retail, business, employee)
Puting it another way, the policy is writting against a logical/abstract model, which doesn't always have a complete set of data. When a decision is required that relies on the a missing section of the model, the PIP resolver will fetch it and populate the model.

Note: Implementation details - Need think about whether the addtional data should be populated into the OPA model, then the same input object provided as a section step. OR whether the input object should carry the additional data.

```
Assuming 
   MS   ->  ( [PDP] -> [OPA] )
                |
                V
              [PIP]
```


## PEP Input Object
This is passed by the caller, then enriched by
```json5
// Input Object
{
    apiVersion: "v1"
    token: "...."
    access-subject: {
        subject-id: "",
        role: "",
    },
    resource: {
        resource-id: "",
        sub-resource-id: ""
    },
    action: {
        action-id: "Service.Function?"
    },
   
    info: {
        //Added by the PIP ??
    }
}
```




## Policy data mapper/resolver
For each missing key/property, the API would be called and passed back into the model
```json5
//PIP Response
{
    key1: {
        href: "http://../api/profile/{subject-id?}"
    },
    key2: {
        href: "http://../api/account/{resource-id?}"
    }
}
```

## Policy data object from PIP
```json5
// Policy data object
{
// ???

}
```

For a single PIP call, what should be returned - eg. Should it cross domains? (Accounts, Customer, Payments) - probably no.
Which side should provide the relationship edge? 
eg. Permission to create BPay on Account X  - Is this payments or Accounts?   - I think the former.
It should be about the Action, not the resource? The action is the unique thing in the domain, resources may be shared.


PIP Query structure 
/v1/{domain}/user/{userid}/resources
/v1/{domain}/resource/{resourceid}/

