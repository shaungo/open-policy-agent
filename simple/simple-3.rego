package simple

import data.number_list

default found = false

found = true {
    input.number == number_list[_]
}