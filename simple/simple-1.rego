package simple

ok {
    1 == 1
}

wrong {
    1 == 3
}

yes {
    2 > 1
}