package simple

number = 1

ok {
    1 == number
}

wrong {
    2 == number
}
