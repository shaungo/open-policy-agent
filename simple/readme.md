# Simple #1 - Simple Policy only

Simple data model that checks if a user is in the list
### Policy -  simple-1.rego
```python
package simple

ok {
    1 == 1
}

wrong {
    1 == 3
}

yes {
    2 > 1
}
```
This policy will be loaded under `data.simple` based on the package name. It always evaluates to true

### opa CLI REPL
```python
$  opa run simple-1.rego
# Querying 'data' provides the full memory model
>  data
{
    "repl": { ... } # Runtime variables
    },
    "simple": {
        "ok": true,
        "yes": true
    }
}

# Querying just the 'simple' policy provides a subset
>  data.simple
"simple": {
    "ok": true,
    "yes": true
}

# A single property can also be queried
> data.simple.ok
    true
```




# Simple #2 - Data used in policy

### Policy - simple-2.rego
```python
package simple

number = 1

# Reference number variable in policy rule
ok {
    1 == number
}

wrong {
    2 == number
}
```

### CLI REPL
```python

$ opa run simple-2.rego
> data.simple
{
  "number": 1,
  "ok": true
}
```


# Simple #3 - Data & input

### DATA
Simple data model that has a list of userids allowed to access a system
```json
{
    "number_list": [
        1,2,3,4    
    ]
}
```

### POLICY 
Simple policy that checks if a user is in the list
```python
    package simple

    import data.number_list
    default found = false

    found {
        #input number in list?
        input.number == number_list[_]
    }
```
### CLI
```python
    $ opa run -w simple-3.rego simple-data.json
    # evaluate the policy with userid:2 (TRUE)
    > data.simple with input as { "number":2 }
    {
        "found": true
    }
    
    # evaluate the policy with userid:8 (FALSE)
    > data.simple with input as { "number":8 }
    {
        "found": false
    }
```


# Loading Example 3 via the REST API

### Load data from a file *using httpie*
        
    $ http PUT localhost:8181/v1/data @simple-3-data.json
    
    HTTP/1.1 204 No Content
    Date: Sat, 21 Jul 2018 13:36:11 GMT


Read the data back: 

    $ http GET localhost:8181/v1/data/number_list

    HTTP/1.1 200 OK
    Content-Length: 22
    Content-Type: application/json
    Date: Sat, 21 Jul 2018 13:39:18 GMT

    {
        "result": [
            1,
            2,
            3,
            4,
            5
        ]
    }

### Load the policy

    $ http PUT localhost:8181/v1/policies/simple @simple-3.rego

    HTTP/1.1 200 OK
    Content-Length: 2
    Content-Type: application/json
    Date: Sat, 21 Jul 2018 13:41:02 GMT

    {}

### Evaluate the policy 
Evaluate with validate number (2)

    $ http POST localhost:8181/v1/data/simple @simple-3-input-OK.json
    HTTP/1.1 200 OK
    Content-Length: 25
    Content-Type: application/json
    Date: Sat, 21 Jul 2018 13:56:57 GMT

    {
        "result": {
            "found": true
        }
    }

Evaluate with missing number (200)

    $ http POST localhost:8181/v1/data/simple @simple-3-input-NOK.json
    HTTP/1.1 200 OK
    Content-Length: 26
    Content-Type: application/json
    Date: Sat, 21 Jul 2018 13:57:02 GMT

    {
        "result": {
            "found": false
        }
    }


