

## Data model

TODO - build a model that has a keyed dictionary, this will be much faster that iterating through an array (I presume)

- [customer_account_data.json](customer_account_data.json) has a 1:1 mapping between account and userid & account ID as an object.

- [account_ownership.json](account_ownership.json) 
has a 1:n model of account objects with an array of owners 

- [customer_account_ownership_data.json](customer_account_ownership_data.json) 
has a 1:m model of user objects with child account arrays


## Input
```json
    {
        "userid":100000000,
        "accountid":11111111,
        "jwt":"NEED TO PUT A REAL ONE HERE",
        "TODO":"REMOVE userid and use the JWT"
    }
```

# Loading data 

Load data from a file *using httpie*
        
    http PUT localhost:8181/v1/data/accounts/account_ownership @account_ownership.json


Read the data back: 

    http GET :8181/v1/data/accounts/account_ownership

### Load the policy

    http PUT localhost:8181/v1/policies/accounts/allow @accounts.rego

Read the policy back:

    http GET localhost:8181/v1/policies/accounts/allow
