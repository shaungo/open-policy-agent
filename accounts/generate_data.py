from faker import Faker
import json
from random import randrange, uniform
import time
import sys

fake = Faker()
fake.seed(4321)

NUMBER_OF_ACCOUNTS = 3
NUMBER_OF_CUSTOMERS = 1000000

def generateCustomer():
    return {
        'userid': int(fake.pydecimal(left_digits=8, right_digits=0, positive=True)),
        'owning_meid': int(fake.pydecimal(left_digits=8, right_digits=0, positive=True)),
        'auth_meid': int(fake.pydecimal(left_digits=8, right_digits=0, positive=True)),
        'accounts':[ {'accountid': fake.sha1() } for x in range(NUMBER_OF_ACCOUNTS) ]
    }
            
def jsonWriter():

    print('{"user_account_ownership": [ ')
    #write an object before the loop to make the ',' formating correct.        
    json.dump(generateCustomer(), sys.stdout )

    for x in range(NUMBER_OF_CUSTOMERS-1):
        print(',')   
        json.dump(generateCustomer(), sys.stdout )
    
    print('\n] } ')
        

# Generates an in memory array and writes at the end. works but uses a lot of memory for lots of customers
def dumpJson():
    userarray = [
            generateCustomer() for x in range(NUMBER_OF_CUSTOMERS)
    ]
        
    jsondata = {
        'user_account_ownership': userarray
    }

    with open('user_account_data.json', 'w') as outfile:
        json.dump(jsondata, outfile)
    outfile.close()





t0 = time.time()
jsonWriter()
#dumpJson()
print >> sys.stderr, time.time() - t0, "seconds"

