


##main.rego
    import data.service_graph
    import data.org_chart

    default allow = false

    allow {
        #defer to the service graph package
        service_graph.allow
    }

##service_graph.rego
    package service_graph
    
    #static data as example only
    service_graph = {
        "landing_page": ["details", "reviews"],
        "review": ["ratings"]
    }

    default allow = false

    #
    allow {
        input.external = true
        input.target = "landing page"
    }

    allow { 
        #what are the allowed targets for this input.source
        allowed_targets = service_graph[input.source]
        #search to see if the target service is in the less
        input.target = allowed_targets[_]
    
    }
}