package main

import (
	"bytes"
	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/json"
	"net/http"
	"strconv"
	"os"
)


var OPA_ADDR = "http://localhost:8181"

//Actual input body, wrapped in the OPAInput type below
type OPANumber struct {
	Number    int `json:"number"`
	Something string
}

//Top level object structure expected by OPA
type OPAInput struct {
	Input OPANumber `json:"input"`
}

//Object to map the OPA response structure to.
type OPAResponse struct {
	Result bool `json:"result"`
}

//Handle request expecting a number provided as a path variable
// check this number against the OPA Policy at path .../simple/found
func main() {
	r := gin.Default()

	ev, exists := os.LookupEnv("OPA_ADDR")
	if exists {
		OPA_ADDR = ev
	}


	r.GET("/simple/:number", func(c *gin.Context) {
		//Get the path variable and parse it
		if n, err := strconv.Atoi(c.Params.ByName("number")); err == nil {

			//Check the provided number against the policy
			if checkNumberPolicy(n) {
				c.JSON(200, "OK")
			} else {
				c.JSON(403, "No good")
			}

		} else {
			//Handle the case where a number isn't provided (Atoi fails)
			c.JSON(400, "not a number")
		}
	})

	r.Run(":8081")
}

//Create the OPA input object with the number provided and call the policy
func checkNumberPolicy(number int) bool {
	//build input object
	input := buildOPAInput(number)
	//OPA input object -> JSON
	inputJson, _ := json.Marshal(input)

	//Invoke the policy by posting the input object
	// expects a bool assigned to the 'result' property of the JSON object

	opaPath := OPA_ADDR + "/v1/data/simple/found"
	if resp, err := http.Post(opaPath, "application/json", bytes.NewBuffer(inputJson)); err == nil {
		//Create a placeholder OPAResponse and parse the response object into it.
		opaResponse := OPAResponse{}
		json.NewDecoder(resp.Body).Decode(&opaResponse)
		//return just the boolean value
		return opaResponse.Result
	} else {
		//Something went wrong calling OPA
		panic("Unable to process OPA Response: " + opaPath)
	}
}

//Construct the OPA input Object.
func buildOPAInput(number int) OPAInput {
	i := &OPAInput{}
	i.Input.Number = number
	i.Input.Something = "???"
	return *i
}
